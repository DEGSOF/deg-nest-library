import { GenericEntity } from './GenericEntity';
export declare class TimedEntity extends GenericEntity {
    createdAt: string;
    updatedAt: string;
    deletedAt: string;
}
