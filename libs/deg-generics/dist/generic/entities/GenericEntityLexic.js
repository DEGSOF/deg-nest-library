"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenericEntityLexic = void 0;
const typeorm_1 = require("typeorm");
const ulid_1 = require("ulid");
class GenericEntityLexic extends typeorm_1.BaseEntity {
    beforeInsert() {
        if (!this.id) {
            this.id = (0, ulid_1.ulid)().toString();
        }
    }
}
__decorate([
    (0, typeorm_1.PrimaryColumn)({ type: 'char', length: 32 }),
    __metadata("design:type", String)
], GenericEntityLexic.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.BeforeInsert)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], GenericEntityLexic.prototype, "beforeInsert", null);
exports.GenericEntityLexic = GenericEntityLexic;
