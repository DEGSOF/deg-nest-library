import { TimedEntityWithLexicId } from './TimedEntityWithLexicId';
export declare class LabedEntityLexic extends TimedEntityWithLexicId {
    label: string;
    code: string;
}
