import { GenericEntityLexic } from './GenericEntityLexic';
export declare class TimedEntityWithLexicId extends GenericEntityLexic {
    createdAt: string;
    updatedAt: string;
    deletedAt: string;
}
