import { TimedEntity } from './TimedEntity';
export declare class LabedEntity extends TimedEntity {
    label: string;
    code: string;
}
