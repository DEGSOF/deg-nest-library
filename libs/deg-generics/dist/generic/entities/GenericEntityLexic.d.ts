import { BaseEntity } from 'typeorm';
export declare class GenericEntityLexic extends BaseEntity {
    id: string;
    beforeInsert(): void;
}
