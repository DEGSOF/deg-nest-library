import { Type } from '@nestjs/common';
import { BaseEntity } from 'typeorm';
import { ICrudController } from '../interfaces/ICrudController.interface';
export declare function ControllerFactory<T extends BaseEntity, C, U, S>(entity: Type<T>, createDto: Type<C>, updateDto: Type<U>): Type<ICrudController<T, C, U, S>>;
