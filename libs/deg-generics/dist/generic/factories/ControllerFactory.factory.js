"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ControllerFactory = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const class_transformer_1 = require("class-transformer");
const AbstractValidationPipe_pipe_1 = require("../pipes/AbstractValidationPipe.pipe");
function ControllerFactory(entity, createDto, updateDto) {
    const createPipe = new AbstractValidationPipe_pipe_1.AbstractValidationPipe({ whitelist: true, transform: true }, { body: createDto });
    const updatePipe = new AbstractValidationPipe_pipe_1.AbstractValidationPipe({ whitelist: true, transform: true }, { body: updateDto });
    class CrudController {
        constructor(service) {
            this.service = service;
        }
        create(body) {
            return __awaiter(this, void 0, void 0, function* () {
                return yield this.service.CreateOrUpdateEntity((yield (0, class_transformer_1.plainToInstance)(entity, body)));
            });
        }
        getOne(id) {
            let where = {};
            where = { id: id };
            return this.service.GetEntityByCriteria({ where: where });
        }
        getAll() {
            return __awaiter(this, void 0, void 0, function* () {
                return yield this.service.GetAllEntities({});
            });
        }
        delete(id) {
            return __awaiter(this, void 0, void 0, function* () {
                let entity = yield this.getOne(id);
                return yield this.service.SoftDeleteEntity(entity, { reload: false });
            });
        }
        update(body) {
            return __awaiter(this, void 0, void 0, function* () {
                return yield this.service.CreateOrUpdateEntity((yield (0, class_transformer_1.plainToInstance)(entity, body)));
            });
        }
    }
    __decorate([
        (0, common_1.Post)(),
        (0, swagger_1.ApiSecurity)('bearer'),
        (0, swagger_1.ApiOperation)({ description: 'Create a new entity' }),
        (0, swagger_1.ApiBody)({ type: createDto, required: true, description: 'Create a new entity', schema: { example: { name: 'test' } } }),
        (0, swagger_1.ApiResponse)({
            status: 201,
            description: 'Create 1 record of entity type',
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            data: {
                                type: 'array'
                            },
                            statusCode: {
                                type: 'number'
                            }
                        },
                        example: {
                            data: 'New ' + entity.name + ' record',
                            statusCode: 201
                        }
                    }
                }
            }
        }),
        (0, swagger_1.ApiResponse)({
            status: 400,
            description: 'Bad request',
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            data: {
                                type: 'array'
                            },
                            statusCode: {
                                type: 'number'
                            }
                        },
                        example: {
                            data: 'Bad request',
                            statusCode: 400
                        }
                    }
                }
            }
        }),
        (0, common_1.UsePipes)(createPipe),
        __param(0, (0, common_1.Body)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Promise)
    ], CrudController.prototype, "create", null);
    __decorate([
        (0, swagger_1.ApiSecurity)('bearer'),
        (0, swagger_1.ApiOperation)({ description: 'Get a entity' }),
        (0, swagger_1.ApiBody)({ type: createDto, required: true, description: 'Create a new entity', schema: { example: { name: 'test' } } }),
        (0, swagger_1.ApiResponse)({
            status: 200,
            description: 'Content of entity'
        }),
        (0, swagger_1.ApiResponse)({
            status: 404,
            description: 'Element not found'
        }),
        (0, common_1.Get)(':id'),
        __param(0, (0, common_1.Param)('id')),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Promise)
    ], CrudController.prototype, "getOne", null);
    __decorate([
        (0, common_1.Get)(),
        (0, swagger_1.ApiSecurity)('bearer'),
        (0, swagger_1.ApiOperation)({ description: 'Get all entities', responses: { '200': { description: 'Array record' } }, parameters: [] }),
        (0, swagger_1.ApiResponse)({
            status: 200,
            description: 'Get all entities',
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            data: {
                                type: 'array'
                            },
                            statusCode: {
                                type: 'number'
                            }
                        },
                        example: {
                            data: [],
                            statusCode: 200
                        }
                    }
                }
            }
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], CrudController.prototype, "getAll", null);
    __decorate([
        (0, swagger_1.ApiSecurity)('bearer'),
        (0, swagger_1.ApiOperation)({ description: 'Create a new entity' }),
        (0, swagger_1.ApiBody)({ type: createDto, required: true, description: 'Create a new entity', schema: { example: { name: 'test' } } }),
        (0, swagger_1.ApiResponse)({
            status: 200,
            description: 'Content deleted'
        }),
        (0, swagger_1.ApiResponse)({
            status: 404,
            description: 'Element not found'
        }),
        (0, common_1.Delete)(':id'),
        __param(0, (0, common_1.Param)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Promise)
    ], CrudController.prototype, "delete", null);
    __decorate([
        (0, swagger_1.ApiSecurity)('bearer'),
        (0, swagger_1.ApiOperation)({ description: 'Create a new entity' }),
        (0, swagger_1.ApiBody)({ type: createDto, required: true, description: 'Create a new entity', schema: { example: { name: 'test' } } }),
        (0, swagger_1.ApiResponse)({
            status: 201,
            description: 'Updated record of entity type'
        }),
        (0, swagger_1.ApiResponse)({
            status: 400,
            description: 'Bad request'
        }),
        (0, common_1.Put)('/:id'),
        (0, common_1.UsePipes)(updatePipe),
        __param(0, (0, common_1.Body)()),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Promise)
    ], CrudController.prototype, "update", null);
    return CrudController;
}
exports.ControllerFactory = ControllerFactory;
