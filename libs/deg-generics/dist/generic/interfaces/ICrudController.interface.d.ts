import { BaseEntity, DeepPartial } from 'typeorm';
export interface ICrudController<EntityType extends BaseEntity, CreateDto, UpdateDto, Service> {
    getOne(id: number | string): Promise<EntityType>;
    create(body: CreateDto): Promise<EntityType>;
    update(body: UpdateDto): Promise<EntityType>;
    delete(id: number | string): Promise<DeepPartial<EntityType>>;
    getAll(): Promise<EntityType[]>;
}
