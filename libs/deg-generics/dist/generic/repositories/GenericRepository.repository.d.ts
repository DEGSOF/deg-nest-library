import { BaseEntity, DeepPartial, FindManyOptions, FindOneOptions, RemoveOptions, Repository, SaveOptions } from 'typeorm';
export declare class GenericRepository<_Entity extends BaseEntity, _CreateDTO, _UpdateDTO> extends Repository<_Entity> {
    CreateOrUpdateManyEntities(entities: DeepPartial<_Entity[]>, options?: SaveOptions): Promise<DeepPartial<_Entity[]>>;
    GetEntities(options?: FindManyOptions<_Entity>): Promise<_Entity[]>;
    GetEntityByCriteria(options: FindOneOptions<_Entity>): Promise<_Entity | null>;
    ExecuteRawQuery(query: string, parameters?: any[]): Promise<any>;
    CreateOrUpdateEntity(entity: DeepPartial<_Entity>, options?: SaveOptions): Promise<_Entity>;
    DeleteManyEntities(entities: _Entity[], options?: RemoveOptions): Promise<_Entity[]>;
    DeleteEntity(entity: _Entity, options?: RemoveOptions): Promise<_Entity>;
    SoftDeleteManyEntities(entities: DeepPartial<_Entity[]>, options?: SaveOptions): Promise<_Entity[]>;
    SoftDeleteEntity(entity: DeepPartial<_Entity>, options: SaveOptions & {
        reload: false;
    }): Promise<DeepPartial<_Entity>>;
}
