import { BaseEntity, DeepPartial, FindManyOptions, FindOneOptions, RemoveOptions, SaveOptions } from 'typeorm';
import { IGenericCommands } from '../interfaces/IGenericCommands.interface';
import { GenericRepository } from '../repositories/GenericRepository.repository';
export declare class GenericService<_Entity extends BaseEntity, _CreateDTO, _UpdateDTO> implements IGenericCommands<_Entity, _CreateDTO, _UpdateDTO> {
    protected readonly genericRepository: GenericRepository<_Entity, _CreateDTO, _UpdateDTO>;
    constructor(genericRepository: GenericRepository<_Entity, _CreateDTO, _UpdateDTO>);
    GetAllEntities(options: FindManyOptions<_Entity>, page?: number): Promise<_Entity[]>;
    FindEntitiesByCriteria(where: FindManyOptions<_Entity>): Promise<_Entity[]>;
    GetEntityByCriteria(options: FindOneOptions<_Entity>): Promise<_Entity>;
    ExecuteRawQuery(query: string, parameters?: any[]): Promise<any>;
    CreateOrUpdateManyEntities(entities: DeepPartial<_Entity[]>, options: SaveOptions & {
        reload: false;
    }): Promise<DeepPartial<_Entity[]>>;
    CreateOrUpdateEntity(entity: DeepPartial<_Entity>, options?: SaveOptions): Promise<_Entity>;
    DeleteManyEntities(entities: _Entity[], options?: RemoveOptions): Promise<_Entity[]>;
    DeleteEntity(entity: _Entity, options?: RemoveOptions): Promise<_Entity>;
    SoftDeleteManyEntities(entities: DeepPartial<_Entity[]>, options?: SaveOptions): Promise<_Entity[]>;
    SoftDeleteEntity(entity: DeepPartial<_Entity>, options: SaveOptions & {
        reload: false;
    }): Promise<DeepPartial<_Entity>>;
}
