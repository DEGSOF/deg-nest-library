"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenericService = void 0;
class GenericService {
    constructor(genericRepository) {
        this.genericRepository = genericRepository;
    }
    GetAllEntities(options, page = 1) {
        options.take = options.take === undefined ? 100 : options.take;
        options.skip = options.take * (page - 1);
        return this.genericRepository.GetEntities(options);
    }
    FindEntitiesByCriteria(where) {
        return this.genericRepository.GetEntities(where);
    }
    GetEntityByCriteria(options) {
        return this.genericRepository.GetEntityByCriteria(options);
    }
    ExecuteRawQuery(query, parameters) {
        return this.genericRepository.ExecuteRawQuery(query, parameters);
    }
    CreateOrUpdateManyEntities(entities, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.genericRepository.CreateOrUpdateManyEntities(entities, options);
        });
    }
    CreateOrUpdateEntity(entity, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.genericRepository.CreateOrUpdateEntity(entity, options);
        });
    }
    DeleteManyEntities(entities, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.genericRepository.DeleteManyEntities(entities, options);
        });
    }
    DeleteEntity(entity, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.genericRepository.DeleteEntity(entity, options);
        });
    }
    SoftDeleteManyEntities(entities, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.genericRepository.SoftDeleteManyEntities(entities, options);
        });
    }
    SoftDeleteEntity(entity, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.genericRepository.SoftDeleteEntity(entity, options);
        });
    }
}
exports.GenericService = GenericService;
