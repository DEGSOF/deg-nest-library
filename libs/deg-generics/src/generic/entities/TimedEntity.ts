import { Exclude } from 'class-transformer';
import { CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from 'typeorm';
import { GenericEntity } from './GenericEntity';

export class TimedEntity extends GenericEntity {
    @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)', name: 'created_at' })
    createdAt: string;
    @UpdateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)', name: 'updated_at' })
    updatedAt: string;
    @DeleteDateColumn({ type: 'timestamp', default: () => null, name: 'deleted_at' })
    deletedAt: string;
}
