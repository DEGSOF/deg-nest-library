import { BaseEntity, BeforeInsert, PrimaryColumn } from 'typeorm';
import { ulid } from 'ulid';

export class GenericEntityLexic extends BaseEntity {
    @PrimaryColumn({ type: 'char', length: 32 })
    id: string;

    @BeforeInsert()
    beforeInsert() {
        if (!this.id) {
            this.id = ulid().toString();
        }
    }
}
