import { CreateDateColumn, DeleteDateColumn, UpdateDateColumn } from 'typeorm';
import { GenericEntityLexic } from './GenericEntityLexic';

export class TimedEntityWithLexicId extends GenericEntityLexic {
    @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)', name: 'created_at' })
    createdAt: string;

    @UpdateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)', name: 'updated_at' })
    updatedAt: string;

    @DeleteDateColumn({ type: 'timestamp', default: () => null, name: 'deleted_at' })
    deletedAt: string;
}
