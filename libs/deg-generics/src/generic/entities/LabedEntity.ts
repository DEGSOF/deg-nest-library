import { Column } from 'typeorm';
import { TimedEntity } from './TimedEntity';

export class LabedEntity extends TimedEntity {
    @Column({ type: 'varchar', length: 128 })
    label: string;
    @Column({ type: 'varchar', length: 128 })
    code: string;
}
