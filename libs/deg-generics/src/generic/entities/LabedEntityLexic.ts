import { Column } from 'typeorm';
import { TimedEntityWithLexicId } from './TimedEntityWithLexicId';

export class LabedEntityLexic extends TimedEntityWithLexicId {
    @Column({ type: 'varchar', length: 128 })
    label: string;
    @Column({ type: 'varchar', length: 128 })
    code: string;
}
