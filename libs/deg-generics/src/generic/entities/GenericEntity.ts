import { BaseEntity, Column, PrimaryGeneratedColumn } from 'typeorm';

export class GenericEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ type: 'boolean', default: true, name: 'can_be_deleted' })
    canBeDeleted: boolean;
}
