import { Body, Delete, Get, Param, Post, Put, Type, UsePipes } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiSecurity } from '@nestjs/swagger';
import { plainToInstance } from 'class-transformer';
import { BaseEntity, DeepPartial } from 'typeorm';
import { ICrudController } from '../interfaces/ICrudController.interface';
import { AbstractValidationPipe } from '../pipes/AbstractValidationPipe.pipe';
import { GenericService } from '../services/GenericService.service';

export function ControllerFactory<T extends BaseEntity, C, U, S>(entity: Type<T>, createDto: Type<C>, updateDto: Type<U>): Type<ICrudController<T, C, U, S>> {
    const createPipe = new AbstractValidationPipe({ whitelist: true, transform: true }, { body: createDto });
    const updatePipe = new AbstractValidationPipe({ whitelist: true, transform: true }, { body: updateDto });

    class CrudController<T extends BaseEntity, C, U, S> implements ICrudController<T, C, U, S> {
        public service: GenericService<T, C, U>;

        constructor(service: GenericService<T, C, U>) {
            this.service = service;
        }

        @Post()
        @ApiSecurity('bearer')
        @ApiOperation({ description: 'Create a new entity' })
        @ApiBody({ type: createDto, required: true, description: 'Create a new entity', schema: { example: { name: 'test' } } })
        @ApiResponse({
            status: 201,
            description: 'Create 1 record of entity type',
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            data: {
                                type: 'array'
                            },
                            statusCode: {
                                type: 'number'
                            }
                        },
                        example: {
                            data: 'New ' + entity.name + ' record',
                            statusCode: 201
                        }
                    }
                }
            }
        })
        @ApiResponse({
            status: 400,
            description: 'Bad request',
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            data: {
                                type: 'array'
                            },
                            statusCode: {
                                type: 'number'
                            }
                        },
                        example: {
                            data: 'Bad request',
                            statusCode: 400
                        }
                    }
                }
            }
        })
        @UsePipes(createPipe)
        async create(@Body() body: C): Promise<T> {
            return await this.service.CreateOrUpdateEntity((await plainToInstance(entity, body)) as unknown as DeepPartial<T>);
        }

        @ApiSecurity('bearer')
        @ApiOperation({ description: 'Get a entity' })
        @ApiBody({ type: createDto, required: true, description: 'Create a new entity', schema: { example: { name: 'test' } } })
        @ApiResponse({
            status: 200,
            description: 'Content of entity'
        })
        @ApiResponse({
            status: 404,
            description: 'Element not found'
        })
        @Get(':id')
        getOne(@Param('id') id: number | String): Promise<T> {
            let where: {} = {};
            where = { id: id };
            return this.service.GetEntityByCriteria({ where: where });
        }

        @Get()
        @ApiSecurity('bearer')
        @ApiOperation({ description: 'Get all entities', responses: { '200': { description: 'Array record' } }, parameters: [] })
        @ApiResponse({
            status: 200,
            description: 'Get all entities',
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            data: {
                                type: 'array'
                            },
                            statusCode: {
                                type: 'number'
                            }
                        },
                        example: {
                            data: [],
                            statusCode: 200
                        }
                    }
                }
            }
        })
        async getAll(): Promise<T[]> {
            return await this.service.GetAllEntities({});
        }

        @ApiSecurity('bearer')
        @ApiOperation({ description: 'Create a new entity' })
        @ApiBody({ type: createDto, required: true, description: 'Create a new entity', schema: { example: { name: 'test' } } })
        @ApiResponse({
            status: 200,
            description: 'Content deleted'
        })
        @ApiResponse({
            status: 404,
            description: 'Element not found'
        })
        @Delete(':id')
        async delete(@Param() id: number | string): Promise<DeepPartial<T>> {
            let entity = await this.getOne(id);
            return await this.service.SoftDeleteEntity(entity as DeepPartial<T>, { reload: false });
        }

        @ApiSecurity('bearer')
        @ApiOperation({ description: 'Create a new entity' })
        @ApiBody({ type: createDto, required: true, description: 'Create a new entity', schema: { example: { name: 'test' } } })
        @ApiResponse({
            status: 201,
            description: 'Updated record of entity type'
        })
        @ApiResponse({
            status: 400,
            description: 'Bad request'
        })
        @Put('/:id')
        @UsePipes(updatePipe)
        async update(@Body() body: U): Promise<T> {
            return await this.service.CreateOrUpdateEntity((await plainToInstance(entity, body)) as unknown as DeepPartial<T>);
        }
    }

    return CrudController;
}
