import { DeepPartial, FindManyOptions, FindOneOptions, RemoveOptions, SaveOptions } from 'typeorm';

export interface IGenericCommands<_Entity, _CreateDTO, _UpdateDTO> {
    GetAllEntities(options?: FindManyOptions<_Entity>): Promise<_Entity[]>;
    FindEntitiesByCriteria(where: FindManyOptions<_Entity>): Promise<_Entity[]>;
    GetEntityByCriteria(options: FindOneOptions<_Entity>): Promise<_Entity | null>;
    ExecuteRawQuery(query: string, parameters?: any[]): Promise<any>;
    CreateOrUpdateManyEntities(
        entities: DeepPartial<_Entity[]>,
        options: SaveOptions & {
            reload: false;
        }
    ): Promise<DeepPartial<_Entity[]>>;
    CreateOrUpdateEntity(entity: DeepPartial<_Entity>, options?: SaveOptions): Promise<_Entity>;
    DeleteManyEntities(entities: _Entity[], options?: RemoveOptions): Promise<_Entity[]>;
    DeleteEntity(entity: _Entity, options?: RemoveOptions): Promise<_Entity>;
    SoftDeleteManyEntities(entities: DeepPartial<_Entity[]>, options?: SaveOptions): Promise<_Entity[]>;
    SoftDeleteEntity(
        entity: DeepPartial<_Entity>,
        options: SaveOptions & {
            reload: false;
        }
    ): Promise<DeepPartial<_Entity>>;
}
