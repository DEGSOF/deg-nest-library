import { BaseEntity, DeepPartial, EntityRepository, FindManyOptions, FindOneOptions, RemoveOptions, Repository, SaveOptions } from 'typeorm';

@EntityRepository()
export class GenericRepository<_Entity extends BaseEntity, _CreateDTO, _UpdateDTO> extends Repository<_Entity> {
    /*****ACCESS TO DATABASE*****/

    async CreateOrUpdateManyEntities(
        entities: DeepPartial<_Entity[]>,
        options: SaveOptions = { reload: true, chunk: 10000, data: null, listeners: true, transaction: true }
    ): Promise<DeepPartial<_Entity[]>> {
        try {
            return await this.save(entities, options);
        } catch (error) {
            throw error;
        }
    }

    async GetEntities(options?: FindManyOptions<_Entity>): Promise<_Entity[]> {
        try {
            return await this.find(options);
        } catch (error) {
            throw error;
        }
    }

    async GetEntityByCriteria(options: FindOneOptions<_Entity>): Promise<_Entity | null> {
        try {
            return this.findOne(options);
        } catch (error) {
            throw error;
        }
    }

    async ExecuteRawQuery(query: string, parameters?: any[]): Promise<any> {
        try {
            return await this.query(query, parameters);
        } catch (error) {
            throw error;
        }
    }

    async CreateOrUpdateEntity(entity: DeepPartial<_Entity>, options?: SaveOptions): Promise<_Entity> {
        try {
            return await this.save(entity, options);
        } catch (error) {
            throw error;
        }
    }

    async DeleteManyEntities(entities: _Entity[], options?: RemoveOptions): Promise<_Entity[]> {
        try {
            return await this.remove(entities, options);
        } catch (error) {
            throw error;
        }
    }

    async DeleteEntity(entity: _Entity, options?: RemoveOptions): Promise<_Entity> {
        try {
            return await this.remove(entity, options);
        } catch (error) {
            throw error;
        }
    }

    async SoftDeleteManyEntities(entities: DeepPartial<_Entity[]>, options?: SaveOptions): Promise<_Entity[]> {
        try {
            return await this.softRemove(entities, options);
        } catch (error) {
            throw error;
        }
    }

    async SoftDeleteEntity(
        entity: DeepPartial<_Entity>,
        options: SaveOptions & {
            reload: false;
        }
    ): Promise<DeepPartial<_Entity>> {
        try {
            return await this.softRemove(entity, options);
        } catch (error) {
            throw error;
        }
    }
}
