import { BaseEntity, DeepPartial, FindManyOptions, FindOneOptions, RemoveOptions, SaveOptions } from 'typeorm';
import { IGenericCommands } from '../interfaces/IGenericCommands.interface';
import { GenericRepository } from '../repositories/GenericRepository.repository';

export class GenericService<_Entity extends BaseEntity, _CreateDTO, _UpdateDTO> implements IGenericCommands<_Entity, _CreateDTO, _UpdateDTO> {
    constructor(protected readonly genericRepository: GenericRepository<_Entity, _CreateDTO, _UpdateDTO>) {}

    GetAllEntities(options: FindManyOptions<_Entity>, page: number = 1): Promise<_Entity[]> {
        options.take = options.take === undefined ? 100 : options.take;
        options.skip = options.take * (page - 1);
        return this.genericRepository.GetEntities(options);
    }

    FindEntitiesByCriteria(where: FindManyOptions<_Entity>): Promise<_Entity[]> {
        return this.genericRepository.GetEntities(where);
    }

    GetEntityByCriteria(options: FindOneOptions<_Entity>): Promise<_Entity> {
        return this.genericRepository.GetEntityByCriteria(options);
    }

    ExecuteRawQuery(query: string, parameters?: any[]): Promise<any> {
        return this.genericRepository.ExecuteRawQuery(query, parameters);
    }

    async CreateOrUpdateManyEntities(entities: DeepPartial<_Entity[]>, options: SaveOptions & { reload: false }): Promise<DeepPartial<_Entity[]>> {
        return await this.genericRepository.CreateOrUpdateManyEntities(entities, options);
    }

    async CreateOrUpdateEntity(entity: DeepPartial<_Entity>, options?: SaveOptions): Promise<_Entity> {
        return await this.genericRepository.CreateOrUpdateEntity(entity, options);
    }
    async DeleteManyEntities(entities: _Entity[], options?: RemoveOptions): Promise<_Entity[]> {
        return await this.genericRepository.DeleteManyEntities(entities, options);
    }
    async DeleteEntity(entity: _Entity, options?: RemoveOptions): Promise<_Entity> {
        return await this.genericRepository.DeleteEntity(entity, options);
    }
    async SoftDeleteManyEntities(entities: DeepPartial<_Entity[]>, options?: SaveOptions): Promise<_Entity[]> {
        return await this.genericRepository.SoftDeleteManyEntities(entities, options);
    }
    async SoftDeleteEntity(entity: DeepPartial<_Entity>, options: SaveOptions & { reload: false }): Promise<DeepPartial<_Entity>> {
        return await this.genericRepository.SoftDeleteEntity(entity, options);
    }
}
