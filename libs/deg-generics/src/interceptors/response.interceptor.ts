import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { instanceToPlain } from 'class-transformer';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
    data: T;
    total: number;
}

@Injectable()
export class ResponseInterceptor<T> implements NestInterceptor<T, Response<T>> {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        if (context['contextType']) {
            if (context['contextType'] === 'graphql') {
                return next.handle().pipe(map((data) => data));
            } else {
                return next.handle().pipe(
                    map(async (data) => {
                        return {
                            data: await instanceToPlain(data),
                            statusCode: data.statusCode
                        };
                    })
                );
            }
        }
    }
}
