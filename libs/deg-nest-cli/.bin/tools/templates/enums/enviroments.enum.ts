export enum Environment {
    DEVELOP = 'DEVELOP',
    PRODUCTION = 'PRODUCTION',
    TEST = 'TEST'
}
