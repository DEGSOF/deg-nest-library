export enum HttpEnums {
    HTTP_GET = 'HTTP_GET',
    HTTP_POST = 'HTTP_POST',
    HTTP_PUT = 'HTTP_PUT',
    HTTP_PATCH = 'HTTP_PATCH',
    HTTP_DELETE = 'HTTP_DELETE'
}
