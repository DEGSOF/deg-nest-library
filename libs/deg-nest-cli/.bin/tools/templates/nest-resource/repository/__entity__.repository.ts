import { EntityRepository } from 'typeorm';
import { __entity__PascalCase__ } from '../entities/__entity__.entity';
import { Create__entity__PascalCase__Dto } from '../dto/create-__entity__.dto';
import { Update__entity__PascalCase__Dto } from '../dto/update-__entity__.dto';
import { GenericRepository } from '@dany-deg/generics';

@EntityRepository(__entity__PascalCase__)
export class __entity__PascalCase__Repository extends GenericRepository<__entity__PascalCase__, Create__entity__PascalCase__Dto, Update__entity__PascalCase__Dto> {}
