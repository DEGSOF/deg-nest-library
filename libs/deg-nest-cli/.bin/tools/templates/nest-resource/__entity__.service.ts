import { Injectable } from '@nestjs/common';
import { Create__entity__PascalCase__Dto } from './dto/create-__entity__.dto';
import { Update__entity__PascalCase__Dto } from './dto/update-__entity__.dto';
import { __entity__PascalCase__Repository } from './repository/__entity__.repository';
import { __entity__PascalCase__ } from './entities/__entity__.entity';
import { GenericService } from '@dany-deg/generics';

@Injectable()
export class __entity__PascalCase__Service extends GenericService<__entity__PascalCase__, Create__entity__PascalCase__Dto, Update__entity__PascalCase__Dto> {
    constructor(private readonly repository: __entity__PascalCase__Repository) {
        super(repository);
    }
}
