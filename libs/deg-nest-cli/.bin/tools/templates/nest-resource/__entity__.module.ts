import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { __entity__PascalCase__Controller } from './__entity__.controller';
import { __entity__PascalCase__Service } from './__entity__.service';
import { __entity__PascalCase__Repository } from './repository/__entity__.repository';
@Module({
    controllers: [__entity__PascalCase__Controller],
    providers: [__entity__PascalCase__Service],
    imports: [TypeOrmModule.forFeature([__entity__PascalCase__Repository])],
    exports: [__entity__PascalCase__Service, TypeOrmModule]
})
export class __entity__PascalCase__Module {}
