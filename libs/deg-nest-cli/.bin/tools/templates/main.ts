import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import * as helmet from 'helmet';
import 'reflect-metadata';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from '@dany-deg/exceptions-handler';
import { ResponseInterceptor } from '@dany-deg/generics';

// generar api con el modelo de datos
async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.setGlobalPrefix('api'); //prefijo

    app.use(
        helmet.contentSecurityPolicy({
            directives: {
                ...helmet.contentSecurityPolicy.getDefaultDirectives(),
                'img-src': ["'self'", 'cdn.jsdelivr.net'],
                'script-src': ["'self'", 'cdn.jsdelivr.net', "'unsafe-inline'"]
            }
        })
    );
    useContainer(app.select(AppModule), {
        fallbackOnErrors: true
    });
    app.useGlobalFilters(new HttpExceptionFilter());
    app.useGlobalInterceptors(new ResponseInterceptor());
    app.useGlobalPipes(
        new ValidationPipe({
            exceptionFactory: (errors) => new BadRequestException(errors),
            whitelist: true,
            transform: true,
            forbidNonWhitelisted: true,
            transformOptions: {
                enableImplicitConversion: true
            }
        })
    );
    const config = new DocumentBuilder()
        .addApiKey(
            {
                type: 'apiKey',
                in: 'header',
                name: 'x-api-key'
            },
            'x-api-key'
        )
        .addSecurityRequirements('x-api-key')
        .setTitle('NodeBackend Generic')
        .setDescription('The API description')
        .setVersion('0.0.1')
        .addTag('generic')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);
    app.enableCors();
    const configService = app.get(ConfigService);
    const port = configService.get('PORT');
    console.log(`Server running on port ${port}`);
    await app.listen(port);
}
bootstrap();
