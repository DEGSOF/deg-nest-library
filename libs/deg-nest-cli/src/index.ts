#!/usr/bin/env node
import inquirer from 'inquirer';
import { generateTemplateFiles, CaseConverterEnum, generateTemplateFilesBatch } from 'generate-template-files';
import * as path from 'path';
process.removeAllListeners('warning');
import { exec } from 'child_process';
import { promisify } from 'util';
let execAsync = promisify(exec);
let { stdout: globalPath } = await execAsync('npm root -g');
globalPath = globalPath.replace(/(\r\n|\n|\r)/gm, '');
var basedir = `${globalPath.toString()}\\@dany-deg\\nest-cli\\.bin\\tools\\templates`;
console.log(basedir);
inquirer
    .prompt([
        {
            type: 'list',
            name: 'action',
            message: 'Good morning sir, What would you like to do today?',
            choices: [
                {
                    name: 'Generate a new DEG-Nest application',
                    value: 'generate'
                },
                {
                    name: 'Generate a entity resource',
                    value: 'entity-config'
                }
            ]
        }
    ])
    .then(async (answers) => {
        switch (answers.action) {
            case 'generate':
                await generateTemplateFilesBatch([
                    {
                        entry: {
                            folderPath: path.join(basedir, './dockerfile')
                        },
                        option: '',
                        defaultCase: CaseConverterEnum.KebabCase,
                        dynamicReplacers: [
                            {
                                slot: '__entity__',
                                slotValue: 'dockerfile'
                            }
                        ],
                        output: {
                            path: './dockerfile',
                            overwrite: true
                        },
                        onComplete: (result) => {
                            console.log(result);
                        }
                    }
                ]);
                await generateTemplateFilesBatch([
                    {
                        entry: {
                            folderPath: path.join(basedir, './docker-compose.yml')
                        },
                        option: '',
                        defaultCase: CaseConverterEnum.KebabCase,
                        dynamicReplacers: [
                            {
                                slot: '__entity__',
                                slotValue: 'docker-compose.yml'
                            }
                        ],
                        output: {
                            path: './docker-compose.yml',
                            overwrite: true
                        },
                        onComplete: (result) => {
                            console.log(result);
                        }
                    }
                ]);
                await generateTemplateFilesBatch([
                    {
                        entry: {
                            folderPath: path.join(basedir, './.env')
                        },
                        option: '',
                        defaultCase: CaseConverterEnum.KebabCase,
                        dynamicReplacers: [
                            {
                                slot: '__entity__',
                                slotValue: '.env'
                            }
                        ],
                        output: {
                            path: './src/.env',
                            overwrite: true
                        },
                        onComplete: (result) => {
                            console.log(result);
                        }
                    }
                ]);
                await generateTemplateFilesBatch([
                    {
                        entry: {
                            folderPath: path.join(basedir, './database')
                        },
                        option: '',
                        defaultCase: CaseConverterEnum.KebabCase,
                        dynamicReplacers: [
                            {
                                slot: '__entity__',
                                slotValue: 'database'
                            }
                        ],
                        output: {
                            path: './src/database',
                            overwrite: true
                        },
                        onComplete: (result) => {
                            console.log(result);
                        }
                    }
                ]);
                await generateTemplateFilesBatch([
                    {
                        entry: {
                            folderPath: path.join(basedir, './enums')
                        },
                        option: '',
                        defaultCase: CaseConverterEnum.KebabCase,
                        dynamicReplacers: [
                            {
                                slot: '__entity__',
                                slotValue: 'enums'
                            }
                        ],
                        output: {
                            path: './src/enums',
                            overwrite: true
                        },
                        onComplete: (result) => {
                            console.log(result);
                        }
                    }
                ]);
                await generateTemplateFilesBatch([
                    {
                        entry: {
                            folderPath: path.join(basedir, './guards')
                        },
                        option: '',
                        defaultCase: CaseConverterEnum.KebabCase,
                        dynamicReplacers: [
                            {
                                slot: '__entity__',
                                slotValue: 'guards'
                            }
                        ],
                        output: {
                            path: './src/guards',
                            overwrite: true
                        },
                        onComplete: (result) => {
                            console.log(result);
                        }
                    }
                ]);
                await generateTemplateFilesBatch([
                    {
                        option: '',
                        entry: {
                            folderPath: path.join(basedir, './main.ts')
                        },
                        defaultCase: CaseConverterEnum.KebabCase,
                        dynamicReplacers: [
                            {
                                slot: '__entity__',
                                slotValue: 'main'
                            }
                        ],
                        output: {
                            path: './src/main.ts',
                            overwrite: true
                        },
                        onComplete: (result) => {
                            console.log(result);
                        }
                    }
                ]);
                break;
            case 'entity-config':
                await generateTemplateFiles([
                    {
                        entry: {
                            folderPath: path.join(basedir, './nest-resource')
                        },
                        option: 'Entity source',
                        defaultCase: CaseConverterEnum.KebabCase,
                        stringReplacers: ['__entity__'],
                        output: {
                            path: './src/modules/__entity__(kebabCase)',
                            overwrite: true
                        },
                        onComplete: (result) => {
                            console.log(result);
                        }
                    }
                ]);
                break;
        }
    })
    .catch((error) => {
        if (error.isTtyError) {
            console.log("Prompt couldn't be rendered in the current environment");
        } else {
            console.log(error);
        }
    });
