#! /usr/bin/env node
import { generateTemplateFiles } from 'generate-template-files';
import path from 'path';
process.removeAllListeners('warning');
var basedir = './node_modules/deg-nest-generator/tools/templates';
export const generate_database_config = () =>
    generateTemplateFiles([
        {
            option: 'Create database config',
            entry: {
                folderPath: path.join(basedir, './database')
            },
            defaultCase: '(kebabCase)',
            dynamicReplacers: [
                {
                    slot: '__entity__',
                    slotValue: 'database'
                }
            ],
            output: {
                path: './src/database',
                overwrite: true
            },
            onComplete: (result) => {
                console.log(result);
            }
        }
    ]);
