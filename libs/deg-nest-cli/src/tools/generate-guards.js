#! /usr/bin/env node
import { generateTemplateFiles } from 'generate-template-files';
import path from 'path';
process.removeAllListeners('warning');
var basedir = './node_modules/deg-nest-generator/tools/templates';
export const generate_guards = () =>
    generateTemplateFiles([
        {
            option: 'Create guards',
            entry: {
                folderPath: path.join(basedir, './guards')
            },
            defaultCase: '(kebabCase)',
            dynamicReplacers: [
                {
                    slot: '__entity__',
                    slotValue: 'guards'
                }
            ],
            output: {
                path: './src/guards',
                overwrite: true
            },
            onComplete: (result) => {
                console.log(result);
            }
        }
    ]);
