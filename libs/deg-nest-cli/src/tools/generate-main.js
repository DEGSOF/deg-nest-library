#! /usr/bin/env node
import { generateTemplateFiles } from 'generate-template-files';
import path from 'path';
process.removeAllListeners('warning');
var basedir = './node_modules/deg-nest-generator/tools/templates';
export const generate_main = () =>
    generateTemplateFiles([
        {
            option: 'Create main',
            entry: {
                folderPath: path.join(basedir, './main.ts')
            },
            defaultCase: '(kebabCase)',
            dynamicReplacers: [
                {
                    slot: '__entity__',
                    slotValue: 'main'
                }
            ],
            output: {
                path: './src/main.ts',
                overwrite: true
            },
            onComplete: (result) => {
                console.log(result);
            }
        }
    ]);
