#! /usr/bin/env node
import { generateTemplateFiles } from 'generate-template-files';
import path from 'path';
process.removeAllListeners('warning');
var basedir = './node_modules/deg-nest-generator/tools/templates';
export const generate_resource = () =>
    generateTemplateFiles([
        {
            option: 'Create Module resource',
            entry: {
                folderPath: path.join(basedir, './nest-resource')
            },
            defaultCase: '(kebabCase)',
            stringReplacers: ['__entity__'],
            output: {
                path: './src/modules/__entity__(kebabCase)',
                overwrite: true
            },
            onComplete: (result) => {
                console.log(result);
            }
        }
    ]);
