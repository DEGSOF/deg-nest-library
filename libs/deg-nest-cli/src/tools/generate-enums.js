#! /usr/bin/env node
import { generateTemplateFiles } from 'generate-template-files';
import path from 'path';
process.removeAllListeners('warning');
var basedir = './node_modules/deg-nest-generator/tools/templates';
export const generate_enums = () =>
    generateTemplateFiles([
        {
            option: 'Create enums',
            entry: {
                folderPath: path.join(basedir, './enums')
            },
            defaultCase: '(kebabCase)',
            dynamicReplacers: [
                {
                    slot: '__entity__',
                    slotValue: 'enums'
                }
            ],
            output: {
                path: './src/enums',
                overwrite: true
            },
            onComplete: (result) => {
                console.log(result);
            }
        }
    ]);
