declare const toSlugCode: () => (target: any, key: string) => void;
declare const valueToSlugCode: (text: string) => string;
export { toSlugCode, valueToSlugCode };
