"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.valueToSlugCode = exports.toSlugCode = void 0;
const class_transformer_1 = require("class-transformer");
const toSlugCode = () => {
    const toPlain = (0, class_transformer_1.Transform)(({ value }) => {
        return value;
    }, { toPlainOnly: true });
    const toClass = (target, key) => {
        return (0, class_transformer_1.Transform)(({ obj }) => {
            return valueToSlugCode(obj[key]);
        }, {
            toClassOnly: true
        })(target, key);
    };
    return function (target, key) {
        toPlain(target, key);
        toClass(target, key);
    };
};
exports.toSlugCode = toSlugCode;
const valueToSlugCode = (text) => {
    text = text.toString().toLowerCase().trim();
    const sets = [
        { to: 'a', from: '[ÀÁÂÃÄÅÆĀĂĄẠẢẤẦẨẪẬẮẰẲẴẶἀ@]' },
        { to: 'c', from: '[ÇĆĈČ]' },
        { to: 'd', from: '[ÐĎĐÞ]' },
        { to: 'e', from: '[ÈÉÊËĒĔĖĘĚẸẺẼẾỀỂỄỆ]' },
        { to: 'g', from: '[ĜĞĢǴ]' },
        { to: 'h', from: '[ĤḦ]' },
        { to: 'i', from: '[ÌÍÎÏĨĪĮİỈỊ]' },
        { to: 'j', from: '[Ĵ]' },
        { to: 'ij', from: '[Ĳ]' },
        { to: 'k', from: '[Ķ]' },
        { to: 'l', from: '[ĹĻĽŁ]' },
        { to: 'm', from: '[Ḿ]' },
        { to: 'n', from: '[ŃŅ]' },
        { to: 'ni', from: '[ÑŇ]' },
        { to: 'o', from: '[ÒÓÔÕÖØŌŎŐỌỎỐỒỔỖỘỚỜỞỠỢǪǬƠ]' },
        { to: 'oe', from: '[Œ]' },
        { to: 'p', from: '[ṕ]' },
        { to: 'r', from: '[ŔŖŘ]' },
        { to: 's', from: '[ßŚŜŞŠȘ]' },
        { to: 't', from: '[ŢŤ]' },
        { to: 'u', from: '[ÙÚÛÜŨŪŬŮŰŲỤỦỨỪỬỮỰƯ]' },
        { to: 'w', from: '[ẂŴẀẄ]' },
        { to: 'x', from: '[ẍ]' },
        { to: 'y', from: '[ÝŶŸỲỴỶỸ]' },
        { to: 'z', from: '[ŹŻŽ]' },
        { to: '-', from: "[·/_,:;']" }
    ];
    sets.forEach((set) => {
        text = text.replace(new RegExp(set.from, 'gi'), set.to);
    });
    return text
        .replace(/\s+/g, '-')
        .replace(/[^-a-zа-я\u0370-\u03ff\u1f00-\u1fff]+/g, '')
        .replace(/--+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
};
exports.valueToSlugCode = valueToSlugCode;
