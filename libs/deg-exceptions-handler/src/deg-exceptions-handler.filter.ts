import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common';
import { GqlExceptionFilter } from '@nestjs/graphql';
import { isArray } from 'class-validator';
@Catch()
export class HttpExceptionFilter implements ExceptionFilter, GqlExceptionFilter {
    errors = [];
    async catch(exception: any, host: ArgumentsHost) {
        this.errors = [];
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest<Request>() === undefined ? null : ctx.getRequest<Request>();
        let statusCode = exception instanceof HttpException ? (exception.getStatus() ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR) : HttpStatus.INTERNAL_SERVER_ERROR;
        let exceptionObject = <Object>exception;
        if (exceptionObject['response']) {
            if (exceptionObject['response']['message']) {
                this.traverse(exceptionObject['response']['message']);
            } else {
                this.traverse(exceptionObject['response']);
            }
        }
        response.status(statusCode).json({
            error: {
                code: statusCode,
                message: exceptionObject['message'],
                timestamp: new Date().toISOString(),
                path: request === null ? '' : request.url === undefined ? '' : request.url,
                details: this.errors
            }
        });
    }
    //recurrent function to sum
    traverse(obj) {
        if (isArray(obj)) {
            obj.forEach((element) => this.traverse(element));
        } else {
            if (obj['property'] && obj['constraints']) {
                this.errors.push({
                    property: obj['property'],
                    constraints: obj['constraints']
                });
            }
            if (obj['children'] && obj['children'].length > 0) {
                this.errors.push({
                    property: obj['property'],
                    constraints: obj.children.map((child) => {
                        return {
                            property: child.property,
                            constraints: child.constraints
                        };
                    })
                });
            }
        }
    }
}
