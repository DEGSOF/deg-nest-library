import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
import { GqlExceptionFilter } from '@nestjs/graphql';
export declare class HttpExceptionFilter implements ExceptionFilter, GqlExceptionFilter {
    errors: any[];
    catch(exception: any, host: ArgumentsHost): Promise<void>;
    traverse(obj: any): void;
}
