"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpExceptionFilter = void 0;
const deg_exceptions_handler_filter_1 = require("./deg-exceptions-handler.filter");
Object.defineProperty(exports, "HttpExceptionFilter", { enumerable: true, get: function () { return deg_exceptions_handler_filter_1.HttpExceptionFilter; } });
