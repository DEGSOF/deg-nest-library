"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const class_validator_1 = require("class-validator");
let HttpExceptionFilter = class HttpExceptionFilter {
    constructor() {
        this.errors = [];
    }
    catch(exception, host) {
        return __awaiter(this, void 0, void 0, function* () {
            this.errors = [];
            const ctx = host.switchToHttp();
            const response = ctx.getResponse();
            const request = ctx.getRequest() === undefined ? null : ctx.getRequest();
            let statusCode = exception instanceof common_1.HttpException ? (exception.getStatus() ? exception.getStatus() : common_1.HttpStatus.INTERNAL_SERVER_ERROR) : common_1.HttpStatus.INTERNAL_SERVER_ERROR;
            let exceptionObject = exception;
            if (exceptionObject['response']) {
                if (exceptionObject['response']['message']) {
                    this.traverse(exceptionObject['response']['message']);
                }
                else {
                    this.traverse(exceptionObject['response']);
                }
            }
            response.status(statusCode).json({
                error: {
                    code: statusCode,
                    message: exceptionObject['message'],
                    timestamp: new Date().toISOString(),
                    path: request === null ? '' : request.url === undefined ? '' : request.url,
                    details: this.errors
                }
            });
        });
    }
    traverse(obj) {
        if ((0, class_validator_1.isArray)(obj)) {
            obj.forEach((element) => this.traverse(element));
        }
        else {
            if (obj['property'] && obj['constraints']) {
                this.errors.push({
                    property: obj['property'],
                    constraints: obj['constraints']
                });
            }
            if (obj['children'] && obj['children'].length > 0) {
                this.errors.push({
                    property: obj['property'],
                    constraints: obj.children.map((child) => {
                        return {
                            property: child.property,
                            constraints: child.constraints
                        };
                    })
                });
            }
        }
    }
};
HttpExceptionFilter = __decorate([
    (0, common_1.Catch)()
], HttpExceptionFilter);
exports.HttpExceptionFilter = HttpExceptionFilter;
