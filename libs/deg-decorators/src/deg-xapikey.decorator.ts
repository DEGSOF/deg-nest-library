import { SetMetadata } from '@nestjs/common';

export const XAPIKEYDECORATOR = (...key: string[]) => SetMetadata('x-api-key', key);
