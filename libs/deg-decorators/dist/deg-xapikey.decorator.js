"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.XAPIKEYDECORATOR = void 0;
const common_1 = require("@nestjs/common");
const XAPIKEYDECORATOR = (...key) => (0, common_1.SetMetadata)('x-api-key', key);
exports.XAPIKEYDECORATOR = XAPIKEYDECORATOR;
