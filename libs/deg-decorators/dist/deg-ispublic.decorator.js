"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicDecorator = void 0;
const common_1 = require("@nestjs/common");
const PublicDecorator = () => (0, common_1.SetMetadata)('isPublic', true);
exports.PublicDecorator = PublicDecorator;
