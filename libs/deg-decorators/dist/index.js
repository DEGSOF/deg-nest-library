"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.XAPIKEYDECORATOR = exports.RolesDecorator = exports.PublicDecorator = void 0;
const deg_ispublic_decorator_1 = require("./deg-ispublic.decorator");
Object.defineProperty(exports, "PublicDecorator", { enumerable: true, get: function () { return deg_ispublic_decorator_1.PublicDecorator; } });
const deg_roles_decorator_1 = require("./deg-roles.decorator");
Object.defineProperty(exports, "RolesDecorator", { enumerable: true, get: function () { return deg_roles_decorator_1.RolesDecorator; } });
const deg_xapikey_decorator_1 = require("./deg-xapikey.decorator");
Object.defineProperty(exports, "XAPIKEYDECORATOR", { enumerable: true, get: function () { return deg_xapikey_decorator_1.XAPIKEYDECORATOR; } });
