export declare enum Environment {
    DEVELOP = "DEVELOP",
    PRODUCTION = "PRODUCTION",
    TEST = "TEST"
}
export declare class EnvironmentVariables {
    NODE_ENV: Environment;
    TYPEORM_CONNECTION: string;
    TYPEORM_HOST: string;
    TYPEORM_PORT: number;
    PORT: number;
    NEED_SQL_LOG: boolean;
    TYPEORM_USERNAME: string;
    TYPEORM_PASSWORD: string;
    TYPEORM_DATABASE: string;
    TYPEORM_AUTO_SCHEMA_SYNC: boolean;
    MASTER_UID: string;
    MULTER_DEST: string;
    TYPEORM_MIGRATIONS_TABLE_NAME: string;
    TYPEORM_ENTITY_PREFIX: string;
    MAIL_HOST: string;
    MAIL_USER: string;
    MAIL_PASSWORD: string;
    MAIL_FROM: string;
}
export declare function validate(config: Record<string, unknown>): EnvironmentVariables;
