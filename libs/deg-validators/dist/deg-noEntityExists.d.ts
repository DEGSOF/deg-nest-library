import { ValidationArguments, ValidationOptions, ValidatorConstraintInterface } from 'class-validator';
import { Connection } from 'typeorm';
export declare class noExistsEntityRule implements ValidatorConstraintInterface {
    private readonly connection;
    constructor(connection: Connection);
    validate(value: number, args: ValidationArguments): Promise<boolean>;
}
export declare function noExistsEntity(entity: Function, column: string, validationOptions?: ValidationOptions): (object: any, propertyName: string) => void;
