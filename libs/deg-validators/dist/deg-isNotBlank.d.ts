import { ValidationOptions } from 'class-validator';
export declare function IsNotBlank(property: string, validationOptions?: ValidationOptions): (object: Object, propertyName: string) => void;
