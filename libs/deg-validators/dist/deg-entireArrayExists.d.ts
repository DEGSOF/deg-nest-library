import { ValidationArguments, ValidationOptions, ValidatorConstraintInterface } from 'class-validator';
import { Connection } from 'typeorm';
export declare class EntireArrayExistRule implements ValidatorConstraintInterface {
    private readonly connection;
    constructor(connection: Connection);
    validate(value: any, args: ValidationArguments): Promise<boolean>;
}
export declare function EntireArrayExist(entity: Function, column: string, validationOptions?: ValidationOptions): (object: any, propertyName: string) => void;
