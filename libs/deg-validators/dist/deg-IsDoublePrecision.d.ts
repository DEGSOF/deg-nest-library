export declare function IsDoublePrecision(params: {
    min: number;
    max: number;
    decimalDigits: number;
}): (object: Record<string, any>, propertyName: string) => void;
