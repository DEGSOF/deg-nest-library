import { ValidationOptions } from 'class-validator';
export declare function IsValidCuitCuil(validationOptions?: ValidationOptions): (object: Record<string, any>, propertyName: string) => void;
