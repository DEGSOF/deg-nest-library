"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntireArrayExist = exports.EntireArrayExistRule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const class_validator_1 = require("class-validator");
const typeorm_2 = require("typeorm");
let EntireArrayExistRule = class EntireArrayExistRule {
    constructor(connection) {
        this.connection = connection;
    }
    validate(value, args) {
        return __awaiter(this, void 0, void 0, function* () {
            const [entity, column] = args.constraints;
            const repository = this.connection.getRepository(entity);
            if (!(0, class_validator_1.isArray)(value)) {
                return false;
            }
            try {
                let result = yield repository.createQueryBuilder().select('count(*)', 'cantidad').where(' id in (:...ids)', { ids: value }).execute();
                if (result[0]) {
                    if (result[0].cantidad == value.length) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            catch (error) {
                throw new common_1.InternalServerErrorException(error);
            }
        });
    }
};
EntireArrayExistRule = __decorate([
    (0, class_validator_1.ValidatorConstraint)({ name: 'EntireArrayExistRule', async: true }),
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectConnection)()),
    __metadata("design:paramtypes", [typeorm_2.Connection])
], EntireArrayExistRule);
exports.EntireArrayExistRule = EntireArrayExistRule;
function EntireArrayExist(entity, column, validationOptions) {
    return function (object, propertyName) {
        (0, class_validator_1.registerDecorator)({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [entity, column],
            validator: EntireArrayExistRule
        });
    };
}
exports.EntireArrayExist = EntireArrayExist;
