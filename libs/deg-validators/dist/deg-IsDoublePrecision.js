"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsDoublePrecision = void 0;
const class_validator_1 = require("class-validator");
let IsDoublePrecisionConstraint = class IsDoublePrecisionConstraint {
    validate(value, params) {
        let min = params.constraints[0].min;
        let max = params.constraints[0].max;
        let decimalDigits = params.constraints[0].decimalDigits;
        if (typeof value === 'number') {
            if (value % 1 === 0) {
                if (value > max) {
                    return false;
                }
                if (value < min) {
                    return false;
                }
                return true;
            }
            if (value > max) {
                return false;
            }
            if (value < min) {
                return false;
            }
            const valueText = value.toString();
            const valueSegments = valueText.split('.');
            const decimals = valueSegments[1];
            return decimals.length <= decimalDigits;
        }
        return false;
    }
    defaultMessage(args) {
        let min = args.constraints[0].min;
        let max = args.constraints[0].max;
        let decimalDigits = args.constraints[0].decimalDigits;
        if (args.value > max) {
            return `${args.property} debe ser menor que ${max}.`;
        }
        if (args.value < min) {
            return `${args.property} debe ser mayor que ${min}.`;
        }
        return `${args.property} debe tener como maximo ${decimalDigits} decimales.`;
    }
};
IsDoublePrecisionConstraint = __decorate([
    (0, class_validator_1.ValidatorConstraint)()
], IsDoublePrecisionConstraint);
function IsDoublePrecision(params) {
    return (object, propertyName) => {
        (0, class_validator_1.registerDecorator)({
            target: object.constructor,
            propertyName,
            constraints: [params],
            validator: IsDoublePrecisionConstraint
        });
    };
}
exports.IsDoublePrecision = IsDoublePrecision;
