"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validate = exports.EnvironmentVariables = exports.Environment = void 0;
const class_transformer_1 = require("class-transformer");
const transforms_1 = require("@dany-deg/transforms");
const class_validator_1 = require("class-validator");
var Environment;
(function (Environment) {
    Environment["DEVELOP"] = "DEVELOP";
    Environment["PRODUCTION"] = "PRODUCTION";
    Environment["TEST"] = "TEST";
})(Environment = exports.Environment || (exports.Environment = {}));
class EnvironmentVariables {
}
__decorate([
    (0, class_validator_1.IsEnum)(Environment),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "NODE_ENV", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "TYPEORM_CONNECTION", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "TYPEORM_HOST", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Number)
], EnvironmentVariables.prototype, "TYPEORM_PORT", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", Number)
], EnvironmentVariables.prototype, "PORT", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, class_transformer_1.Expose)(),
    (0, transforms_1.ToBoolean)(),
    __metadata("design:type", Boolean)
], EnvironmentVariables.prototype, "NEED_SQL_LOG", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "TYPEORM_USERNAME", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "TYPEORM_PASSWORD", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "TYPEORM_DATABASE", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    (0, class_transformer_1.Expose)(),
    (0, transforms_1.ToBoolean)(),
    __metadata("design:type", Boolean)
], EnvironmentVariables.prototype, "TYPEORM_AUTO_SCHEMA_SYNC", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "MASTER_UID", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "MULTER_DEST", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "TYPEORM_MIGRATIONS_TABLE_NAME", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "TYPEORM_ENTITY_PREFIX", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "MAIL_HOST", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "MAIL_USER", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    (0, class_transformer_1.Expose)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "MAIL_PASSWORD", void 0);
__decorate([
    (0, class_transformer_1.Expose)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], EnvironmentVariables.prototype, "MAIL_FROM", void 0);
exports.EnvironmentVariables = EnvironmentVariables;
function validate(config) {
    const validatedConfig = (0, class_transformer_1.plainToClass)(EnvironmentVariables, config, {
        excludeExtraneousValues: true,
        enableImplicitConversion: true
    });
    console.log(validatedConfig.NEED_SQL_LOG);
    const errors = (0, class_validator_1.validateSync)(validatedConfig, {
        skipMissingProperties: false,
        forbidNonWhitelisted: true
    });
    if (errors.length > 0) {
        throw new Error(errors.toString());
    }
    return validatedConfig;
}
exports.validate = validate;
