import { registerDecorator, ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint()
class IsDoublePrecisionConstraint implements ValidatorConstraintInterface {
    public validate(value: any, params): boolean {
        let min = params.constraints[0].min;
        let max = params.constraints[0].max;
        let decimalDigits = params.constraints[0].decimalDigits;

        if (typeof value === 'number') {
            if (value % 1 === 0) {
                if (value > max) {
                    return false;
                }

                if (value < min) {
                    return false;
                }

                return true;
            }

            if (value > max) {
                return false;
            }

            if (value < min) {
                return false;
            }

            const valueText: string = value.toString();
            const valueSegments: string[] = valueText.split('.');
            const decimals: string = valueSegments[1];
            return decimals.length <= decimalDigits;
        }

        return false;
    }

    public defaultMessage(args: ValidationArguments): string {
        let min = args.constraints[0].min;
        let max = args.constraints[0].max;
        let decimalDigits = args.constraints[0].decimalDigits;
        if (args.value > max) {
            return `${args.property} debe ser menor que ${max}.`;
        }

        if (args.value < min) {
            return `${args.property} debe ser mayor que ${min}.`;
        }

        return `${args.property} debe tener como maximo ${decimalDigits} decimales.`;
    }
}

export function IsDoublePrecision(params: { min: number; max: number; decimalDigits: number }) {
    return (object: Record<string, any>, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName,
            constraints: [params],
            validator: IsDoublePrecisionConstraint
        });
    };
}
