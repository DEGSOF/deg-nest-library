import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { Connection } from 'typeorm';

@ValidatorConstraint({ name: 'noExistsEntityRule', async: true })
@Injectable()
export class noExistsEntityRule implements ValidatorConstraintInterface {
    constructor(@InjectConnection() private readonly connection: Connection) {}

    async validate(value: number, args: ValidationArguments) {
        const [entity, column] = args.constraints;
        const repository = this.connection.getRepository(entity);
        try {
            const record = await repository.findOne({ where: { [column]: value } }).then((rec) => {
                return rec;
            });
            return record === undefined;
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }
}

export function noExistsEntity(entity: Function, column: string, validationOptions?: ValidationOptions) {
    return function (object: any, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [entity, column],
            validator: noExistsEntityRule
        });
    };
}
