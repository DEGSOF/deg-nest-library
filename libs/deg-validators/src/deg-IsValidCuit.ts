import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';

@ValidatorConstraint({ async: true })
class IsValidCuitCuilConstraint implements ValidatorConstraintInterface {
    async validate(value: any | Array<any>, args: ValidationArguments) {
        if (!value) return false;
        if (value.length != 11) return false;
        let rv = false;
        let resultado = 0;
        let cuit_nro = value.replace('-', '');
        const codes = '6789456789';
        let verificador = parseInt(cuit_nro[cuit_nro.length - 1]);
        let x = 0;

        while (x < 10) {
            let digitoValidador = parseInt(codes.substring(x, x + 1));
            if (isNaN(digitoValidador)) digitoValidador = 0;
            let digito = parseInt(cuit_nro.substring(x, x + 1));
            if (isNaN(digito)) digito = 0;
            let digitoValidacion = digitoValidador * digito;
            resultado += digitoValidacion;
            x++;
        }

        resultado = resultado % 11;
        rv = resultado == verificador;
        return rv;
    }

    defaultMessage(args: ValidationArguments) {
        const property = args.property;
        return `${property} es invalido.`;
    }
}

export function IsValidCuitCuil(validationOptions?: ValidationOptions) {
    return function (object: Record<string, any>, propertyName: string): void {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            validator: IsValidCuitCuilConstraint
        });
    };
}
