import { Expose, plainToClass } from 'class-transformer';
import { ToBoolean } from '@dany-deg/transforms';
import { IsBoolean, IsEnum, IsNumber, IsString, validateSync } from 'class-validator';

export enum Environment {
    DEVELOP = 'DEVELOP',
    PRODUCTION = 'PRODUCTION',
    TEST = 'TEST'
}

export class EnvironmentVariables {
    @IsEnum(Environment)
    @Expose()
    NODE_ENV: Environment;
    @IsString()
    @Expose()
    TYPEORM_CONNECTION: string;
    @IsString()
    @Expose()
    TYPEORM_HOST: string;
    @IsNumber()
    @Expose()
    TYPEORM_PORT: number;
    @IsNumber()
    @Expose()
    PORT: number;
    @IsBoolean()
    @Expose()
    @ToBoolean()
    NEED_SQL_LOG: boolean;
    @IsString()
    @Expose()
    TYPEORM_USERNAME: string;
    @IsString()
    @Expose()
    TYPEORM_PASSWORD: string;
    @IsString()
    @Expose()
    TYPEORM_DATABASE: string;
    @IsBoolean()
    @Expose()
    @ToBoolean()
    TYPEORM_AUTO_SCHEMA_SYNC: boolean;
    @IsString()
    @Expose()
    MASTER_UID: string;
    @IsString()
    @Expose()
    MULTER_DEST: string;
    @IsString()
    @Expose()
    TYPEORM_MIGRATIONS_TABLE_NAME: string;
    @IsString()
    @Expose()
    TYPEORM_ENTITY_PREFIX: string;
    @IsString()
    @Expose()
    MAIL_HOST: string;
    @IsString()
    @Expose()
    MAIL_USER: string;
    @IsString()
    @Expose()
    MAIL_PASSWORD: string;
    @Expose()
    @IsString()
    MAIL_FROM: string;
}

export function validate(config: Record<string, unknown>) {
    const validatedConfig = plainToClass(EnvironmentVariables, config, {
        excludeExtraneousValues: true,
        enableImplicitConversion: true
    });
    console.log(validatedConfig.NEED_SQL_LOG);
    const errors = validateSync(validatedConfig, {
        skipMissingProperties: false,
        forbidNonWhitelisted: true
    });
    if (errors.length > 0) {
        throw new Error(errors.toString());
    }
    return validatedConfig;
}
