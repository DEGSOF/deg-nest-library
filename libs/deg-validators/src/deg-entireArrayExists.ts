import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { isArray, registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { Connection } from 'typeorm';

@ValidatorConstraint({ name: 'EntireArrayExistRule', async: true })
@Injectable()
export class EntireArrayExistRule implements ValidatorConstraintInterface {
    constructor(@InjectConnection() private readonly connection: Connection) {}

    async validate(value: any, args: ValidationArguments) {
        const [entity, column] = args.constraints;
        const repository = this.connection.getRepository(entity);
        if (!isArray(value)) {
            return false;
        }
        try {
            let result = await repository.createQueryBuilder().select('count(*)', 'cantidad').where(' id in (:...ids)', { ids: value }).execute();
            if (result[0]) {
                if (result[0].cantidad == value.length) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }
}
export function EntireArrayExist(entity: Function, column: string, validationOptions?: ValidationOptions) {
    return function (object: any, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [entity, column],
            validator: EntireArrayExistRule
        });
    };
}
