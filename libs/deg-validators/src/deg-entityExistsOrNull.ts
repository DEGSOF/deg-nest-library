import { InternalServerErrorException } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { registerDecorator, ValidationArguments, ValidationOptions, ValidatorConstraint, ValidatorConstraintInterface } from 'class-validator';
import { Connection } from 'typeorm';

@ValidatorConstraint({ name: 'entityExistRule', async: true })
@Injectable()
export class entityExistOrNullRule implements ValidatorConstraintInterface {
    constructor(@InjectConnection() private readonly connection: Connection) {}

    async validate(value: number, args: ValidationArguments) {
        const [entity, column] = args.constraints;
        const repository = this.connection.getRepository(entity);
        if (value) {
            try {
                const record = await repository.findOne({ where: { [column]: value } }).then((rec) => {
                    return rec;
                });
                return record !== undefined;
            } catch (error) {
                throw new InternalServerErrorException(error);
            }
        } else {
            if (value != undefined && value.toString() == '') {
                return false;
            }
            return true;
        }
    }
}

export function EntityExistsOrNull(entity: Function, column: string, validationOptions?: ValidationOptions) {
    return function (object: any, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [entity, column],
            validator: entityExistOrNullRule
        });
    };
}
