/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.HttpExceptionFilter = void 0;
const common_1 = __webpack_require__(2);
const class_validator_1 = __webpack_require__(3);
let HttpExceptionFilter = class HttpExceptionFilter {
    constructor() {
        this.errors = [];
    }
    traverse(forest) {
        if ((0, class_validator_1.isArray)(forest)) {
            forest === null || forest === void 0 ? void 0 : forest.forEach(function (tree) {
                if (tree.constraints) {
                    this.errors.push({
                        property: tree.property,
                        constraints: tree.constraints
                    });
                }
                if (tree.children) {
                    this.traverse(tree.children);
                }
            });
        }
    }
    catch(exception, host) {
        return __awaiter(this, void 0, void 0, function* () {
            const ctx = host.switchToHttp();
            const response = ctx.getResponse();
            const request = ctx.getRequest() === undefined ? null : ctx.getRequest();
            let statusCode = exception instanceof common_1.HttpException ? (exception.getStatus() ? exception.getStatus() : common_1.HttpStatus.INTERNAL_SERVER_ERROR) : common_1.HttpStatus.INTERNAL_SERVER_ERROR;
            let exceptionObject = exception;
            if (exceptionObject['response']) {
                if (exceptionObject['response']['message']) {
                    this.traverse(exceptionObject['response']['message']);
                }
                else {
                    this.traverse(exceptionObject['response']);
                }
            }
            response.status(statusCode).json({
                error: {
                    code: statusCode,
                    message: exceptionObject['message'],
                    timestamp: new Date().toISOString(),
                    path: request === null ? '' : request.url === undefined ? '' : request.url,
                    details: this.errors
                }
            });
        });
    }
};
HttpExceptionFilter = __decorate([
    (0, common_1.Catch)()
], HttpExceptionFilter);
exports.HttpExceptionFilter = HttpExceptionFilter;


/***/ }),
/* 2 */
/***/ ((module) => {

module.exports = require("@nestjs/common");

/***/ }),
/* 3 */
/***/ ((module) => {

module.exports = require("class-validator");

/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.HttpExceptionFilter = void 0;
const deg_exceptions_handler_filter_1 = __webpack_require__(1);
Object.defineProperty(exports, "HttpExceptionFilter", ({ enumerable: true, get: function () { return deg_exceptions_handler_filter_1.HttpExceptionFilter; } }));

})();

/******/ })()
;